FTS Web Monitoring
==================

The FTS3 Web Monitoring allows peeking at the internal FTS3 state: active transfers, success rate, optimizer decisions...

It is *not* a tool for historical data, data aggregation, etc... For this sort of uses, better use the [FTS Dashboard](http://dashb-fts-transfers.cern.ch/ui/). 